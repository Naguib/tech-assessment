<?php
/**
 * @Author Mostafa Naguib
 * @Copyright Maximum Develop
 * @FileCreated 5/2/20 7:15 AM
 * @Contact http://www.max-dev.com/Mostafa.Naguib
 */

if(!function_exists('getCommand')){
    function getCommand(){
        $handle = fopen("php://stdin", "r");
        $line = fgets($handle);
        return trim($line);
    }
}

function flatten_array(array $array) {
    $values = array();
    array_walk_recursive($array, function($val) use (&$values) { $values[] = $val; });
    return $values;
}
