<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //for some security concerns we are not uploading the file, and just using the temp one.
    if($_FILES['textfile']['type'] == 'text/plain') {
        $file = $_FILES['textfile']['tmp_name'];
        $taskOne = new \Olx\Tasks\TaskOne();
        $taskOne->web(!empty($file) ? $file : null);
    } else
        header("Location: index.php?task=One");
} else {
    include_once __DIR__.'/../../views/TaskOne.html';
}
