#OLX tasks

## Requirements

 * PHP ^7.0

 * Apache

## Installing

 * git clone repo

 * composer install

## Running Cli Mode

`php index.php`

### Task One

 * `php index.php`

 * `1`

 * Click return or full path file for example (/home/user/documents/file.txt)

### Task Two

 * `php index.php`

 * `2`

 * `page1` return

 * `page4` return

### Task Three

 * `php index.php`
 * `3`
 * `y` return
 
## Running web mode
Just open it on web browser and follow links 
